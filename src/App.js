import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';

import AppBar from './components/AppBar';
import AppBarBottom from './components/AppBarBottom';
import MenuProvider from './containers/MenuProvider';
import PokemonListData from './containers/PokemonListData';

const client = new ApolloClient({
  uri: 'https://graphql-pokemon.now.sh/graphql'
})

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  container: {
    margin: theme.spacing.unit * 3,
  },
});

const App = React.memo(props => (
  <ApolloProvider client={client}>
    <CssBaseline />
    <MenuProvider>
      <AppBar />
    </MenuProvider>
    <div className={props.classes.appBarSpacer} />
    <div className={props.classes.container}>
      <PokemonListData />
    </div>
    <div className={props.classes.appBarSpacer} />
    <AppBarBottom />
  </ApolloProvider>
));

export default withStyles(styles)(App);
