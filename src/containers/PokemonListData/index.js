import React from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

import LoadingIndicator from '../../components/LoadingIndicator';
import PokemonList from '../../components/PokemonList';

const PokemonListData = () => (
  <Query query={POKEMONLIST_QUERY}>
    {({ loading, error, data}) => (
      <>
        {loading && (
          <LoadingIndicator />
        )}
        {error && (
          <>{error}</>
        )}
        {data.pokemons && <PokemonList pokemons={data.pokemons} />}
      </>
    )}
  </Query>
);

const POKEMONLIST_QUERY = gql`
  query PokemonListQuery {
    pokemons(first: 20) {
      id
      number
      name
      classification
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
    }
  }
`;

export default PokemonListData;
