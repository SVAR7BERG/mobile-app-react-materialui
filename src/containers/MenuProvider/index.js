import React from 'react';

import Menu from '../../components/Menu';

const MenuContext = React.createContext();

class MenuProvider extends React.Component {
  constructor() {
    super();

    this.toggleMenu = this.toggleMenu.bind(this);

    this.state = {
      menuIsOpen: false,
      toggleMenu: this.toggleMenu,
    }
  }

  toggleMenu() {
    this.setState(state => ({
      menuIsOpen: !state.menuIsOpen,
    }));
  }

  render() {
    const { children } = this.props;

    return (
      <MenuContext.Provider value={this.state}>
        <Menu />
        {children}
      </MenuContext.Provider>
    )
  };
};

export default MenuProvider;
export const MenuConsumer = MenuContext.Consumer;
