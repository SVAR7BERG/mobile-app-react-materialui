import React from 'react';

import { withStyles } from '@material-ui/core/styles'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import SettingsIcon from '@material-ui/icons/Settings';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import HotelIcon from '@material-ui/icons/Hotel';
import InfoIcon from '@material-ui/icons/Info';
import HomeIcon from '@material-ui/icons/Home';
import { MenuConsumer } from '../../containers/MenuProvider';

const styles= () => ({
  list: {
    width: 280,
  },
});

const Menu = classes => (
  <MenuConsumer>
    {({ menuIsOpen, toggleMenu }) => (
      <SwipeableDrawer
        open={menuIsOpen}
        onClose={toggleMenu}
        onOpen={toggleMenu}
      >
        <div className={classes.list}>
          <List component="nav">
            <ListItem button>
              <ListItemIcon>
                <HomeIcon />
              </ListItemIcon>
              <ListItemText primary="Home" />
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem button>
              <ListItemIcon>
                <LocationCityIcon />
              </ListItemIcon>
              <ListItemText primary="Entdecken & Erleben" />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <HotelIcon />
              </ListItemIcon>
              <ListItemText primary="Übernachten & Planen" />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <InfoIcon />
              </ListItemIcon>
              <ListItemText primary="Unsere Stadt" />
            </ListItem>
          </List>
          <Divider />
          <List component="nav">
            <ListItem button>
              <ListItemIcon>
                <SettingsIcon />
              </ListItemIcon>
              <ListItemText primary="Einstellungen" />
            </ListItem>
          </List>
        </div>
      </SwipeableDrawer>
    )}
  </MenuConsumer>
);

export default withStyles(styles)(Menu);
