import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import HotelIcon from '@material-ui/icons/Hotel';
import InfoIcon from '@material-ui/icons/Info';

const styles = {
  appBarBottom: {
    top: 'auto',
    bottom: 0,
  },
};

const AppBarBottom = React.memo(props => (
  <AppBar className={props.classes.appBarBottom}>
    <BottomNavigation showLabels>
      <BottomNavigationAction
        label="Entdecken"
        icon={<LocationCityIcon />} 
      />
      <BottomNavigationAction
        label="Planen"
        icon={<HotelIcon />} 
      />
      <BottomNavigationAction
        label="Die Stadt"
        icon={<InfoIcon />} 
      />
    </BottomNavigation>
  </AppBar>
));

export default withStyles(styles)(AppBarBottom);
