import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import MuiAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChatIcon from '@material-ui/icons/Chat';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import { MenuConsumer } from '../../containers/MenuProvider';

const styles = {
  menuButton: {
    marginRight: 16,
  },
  grow: {
    flexGrow: 1,
  },
};

const AppBar = React.memo(props => (
  <MuiAppBar>
    <Toolbar>
      <IconButton className={props.classes.menuButton} color="inherit" aria-label="Menu">
        <MenuConsumer>
          {({ toggleMenu }) => (
            <MenuIcon onClick={toggleMenu} />
          )}
        </MenuConsumer>
      </IconButton>
      <Typography variant="h6" color="inherit">KEPTN</Typography>
      <div className={props.classes.grow} />
      <IconButton color="inherit">
        <Badge badgeContent={4} color="secondary">
          <ChatIcon />
        </Badge>
      </IconButton>
    </Toolbar>
  </MuiAppBar>
));

export default withStyles(styles)(AppBar);
