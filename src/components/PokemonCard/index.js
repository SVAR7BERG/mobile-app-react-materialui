import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';

const styles = theme => ({
  avatar: {
    width: 64,
    height: 64,
  },
  card: {
    maxWidth: 400,
    marginBottom: theme.spacing.unit * 3,
  },
});

const PokemonCard = (props) => {
  const { classes, image, name, classification, weight, height } = props;

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar alt={name} src={image} className={classes.avatar} />
        }
        title={name}
        subheader={classification}
      />
      <CardContent>
        <Typography component="p">
          {`Weights from ${weight.minimum} to ${weight.maximum} and is from ${height.minimum} to ${height.maximum} tall.`}
        </Typography>
      </CardContent>
      <CardActions disableActionSpacing>
        <IconButton aria-label="Add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="Share">
          <ShareIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
}

PokemonCard.propTypes = {
  classes: PropTypes.object,
  image: PropTypes.string,
  name: PropTypes.string,
  classification: PropTypes.string,
  weight: PropTypes.objectOf(PropTypes.string),
  height: PropTypes.objectOf(PropTypes.string),
};

export default withStyles(styles)(PokemonCard);
