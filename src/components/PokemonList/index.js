import React from 'react';

import PokemonCard from '../PokemonCard';

const PokemonList = ({ pokemons }) => (
  <>
    {pokemons.map(({ id, number, name, classification, weight, height }) => (
      <PokemonCard
        key={id}
        name={name}
        image={`https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${number}.png`}
        classification={classification}
        weight={weight}
        height={height}
      />
    ))}
  </>
);

export default PokemonList;
