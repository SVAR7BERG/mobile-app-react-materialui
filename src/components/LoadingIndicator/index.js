import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = () => ({
  center: {
    display: 'block',
    margin: '0 auto',
  },
});

const LoadingIndicator = props => <CircularProgress className={props.classes.center} />;

export default withStyles(styles)(LoadingIndicator);
